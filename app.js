var express = require('express');
var app = express();

require('./apps/kernel.js')(express,app);
require('./routes/web.js')(app);

module.exports = app;