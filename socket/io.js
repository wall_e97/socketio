var socket = require('socket.io');
module.exports = function(server){
    var io = socket(server);

    io.on('connection', function(client){
        client.on('SEND-Message', function(mess){
            // console.log(mess);
            io.emit('SEND-Message', mess);
        });
    
        console.log('Client connect');
    });
}